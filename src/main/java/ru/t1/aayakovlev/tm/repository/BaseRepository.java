package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractModel;

import java.util.List;

public interface BaseRepository<M extends AbstractModel> {

    void clear();

    @NotNull
    List<M> findAll();

    @Nullable
    M findById(@NotNull final String id) throws AbstractException;

    @NotNull
    M remove(@NotNull final M model) throws AbstractException;

    @NotNull
    M removeById(@NotNull final String id) throws AbstractException;

    void removeAll(@NotNull final List<M> models);

    @NotNull
    M save(@NotNull final M model) throws EntityEmptyException;

}
