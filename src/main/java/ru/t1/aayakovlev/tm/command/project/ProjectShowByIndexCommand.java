package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Project;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show project by index.";

    @NotNull
    public static final String NAME = "project-show-by-index";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        @NotNull final Integer index = nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @Nullable final Project project = getProjectService().findByIndex(userId, index);
        showProject(project);
    }

}
