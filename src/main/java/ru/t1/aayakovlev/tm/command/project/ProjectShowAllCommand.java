package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectShowAllCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show all projects";

    @NotNull
    public static final String NAME = "project-show-all";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW ALL PROJECTS]");
        System.out.println("Sorts to enter:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort);
        renderProjects(projects);
    }

}
