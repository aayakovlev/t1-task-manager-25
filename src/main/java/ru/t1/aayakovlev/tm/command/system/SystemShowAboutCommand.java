package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class SystemShowAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String NAME = "about";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("email: " + getPropertyService().getAuthorEmail());
        System.out.println("name: " + getPropertyService().getAuthorName());
    }

}
