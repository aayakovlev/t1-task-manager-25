package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    public static final String NAME = "task-start-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
    }

}
