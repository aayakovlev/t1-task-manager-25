package ru.t1.aayakovlev.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;

@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements WBS {

    public Project(@NotNull final String name, @NotNull final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

}
