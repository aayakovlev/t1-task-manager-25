package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements WBS {

    @Nullable
    private String projectId;

    public Task(@NotNull final String name, @NotNull final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public Task(@NotNull final String name, @NotNull final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

}
