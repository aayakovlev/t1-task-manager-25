package ru.t1.aayakovlev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aayakovlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.*;

import java.lang.reflect.Modifier;
import java.util.Set;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.*;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final CommandRepository commandRepository = new CommandRepositoryImpl();

    @Getter
    @NotNull
    private final CommandService commandService = new CommandServiceImpl(commandRepository);

    @Getter
    @NotNull
    private final LoggerService loggerService = new LoggerServiceImpl();

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @Getter
    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @Getter
    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @Getter
    @NotNull
    private final UserService userService = new UserServiceImpl(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final AuthService authService = new AuthServiceImpl(propertyService, userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> commands = reflections.getSubTypesOf(AbstractCommand.class);
        commands.forEach(this::registry);
    }

    private void initData() throws AbstractException {
        final User first = userService.create("first", "first", "first@first.com");
        final User second = userService.create("second", "second", "second@second.com");
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.save(first.getId(), new Project("project-1", "description"));
        projectService.save(first.getId(), new Project("project-2", Status.COMPLETED));
        projectService.save(second.getId(), new Project("project-3", Status.IN_PROGRESS));
        projectService.save(admin.getId(), new Project("project-4", Status.COMPLETED));

        taskService.save(first.getId(), new Task("task-1", "description"));
        taskService.save(first.getId(), new Task("task-2", Status.IN_PROGRESS));
        taskService.save(second.getId(), new Task("task-3", Status.COMPLETED));
        taskService.save(second.getId(), new Task("task-4", Status.IN_PROGRESS));
        taskService.save(first.getId(), new Task("task-5", Status.COMPLETED));
        taskService.save(admin.getId(), new Task("task-6", "description"));
    }

    private void initLogger() {
        loggerService.info(
                "___________              __                                                        \n" +
                "\\__    ___/____    _____|  | __   _____ _____    ____ _____     ____   ___________ \n" +
                "  |    |  \\__  \\  /  ___/  |/ /  /     \\\\__  \\  /    \\\\__  \\   / ___\\_/ __ \\_  __ \\\n" +
                "  |    |   / __ \\_\\___ \\|    <  |  Y Y  \\/ __ \\|   |  \\/ __ \\_/ /_/  >  ___/|  | \\/\n" +
                "  |____|  (____  /____  >__|_ \\ |__|_|  (____  /___|  (____  /\\___  / \\___  >__|   \n" +
                "               \\/     \\/     \\/       \\/     \\/     \\/     \\//_____/      \\/");
        Runtime.getRuntime().addShutdownHook(new Thread(
                () -> loggerService.info("*** APPLICATION SHUTTING DOWN ***"))
        );
    }

    private boolean processArguments(@Nullable final String[] arguments) throws AbstractException {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return false;
        @Nullable final String argument = arguments[FIRST_ARRAY_ELEMENT_INDEX];
        return processArgument(argument);
    }

    private boolean processArgument(@Nullable final String argument) throws AbstractException {
        if (argument == null || argument.isEmpty()) return false;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
        return true;
    }

    private void processCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        try {
            @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
            registry(command);
        } catch (final @NotNull ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) throws AbstractException {
        if (processArguments(args)) return;
        initData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command: ");
                @Nullable final String command = nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final AbstractException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
