package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.*;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

public final class UserServiceImpl extends AbstractBaseService<User, UserRepository> implements UserService {

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final PropertyService propertyService;

    @NotNull
    private final TaskRepository taskRepository;

    public UserServiceImpl(
            @NotNull final PropertyService propertyService,
            @NotNull final UserRepository repository,
            @NotNull final ProjectRepository projectRepository,
            @NotNull final TaskRepository taskRepository) {
        super(repository);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return save(user);
    }

    @Override
    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new UserEmailExistsException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    @NotNull
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = repository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    @NotNull
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExists(email);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExists(login);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    @NotNull
    public User remove(@Nullable final User model) throws AbstractException {
        @NotNull final User user = super.remove(model);
        @NotNull final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        remove(user);
    }

    @Override
    @NotNull
    public User removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final User user = findByEmail(email);
        return remove(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

}
