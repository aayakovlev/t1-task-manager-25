package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

public interface CommandService {

    void add(@Nullable final AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable final String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String name);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
