package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.AbstractEntityException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;
import ru.t1.aayakovlev.tm.service.BaseService;

import java.util.List;

public abstract class AbstractBaseService<M extends AbstractModel, R extends BaseRepository<M>>
        implements BaseService<M> {

    @NotNull
    protected final R repository;

    public AbstractBaseService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        this.repository.clear();
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public M findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    @NotNull
    public M remove(@Nullable final M model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Override
    public void removeAll(@NotNull final List<M> models) {
        this.repository.removeAll(models);
    }

    @Override
    @NotNull
    public M removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = findById(id);
        return remove(model);
    }

    @Override
    @NotNull
    public M save(@Nullable final M model) throws EntityEmptyException {
        if (model == null) throw new EntityEmptyException();
        return repository.save(model);
    }

}
