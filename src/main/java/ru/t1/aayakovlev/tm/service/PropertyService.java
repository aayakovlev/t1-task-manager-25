package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface PropertyService extends SaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
